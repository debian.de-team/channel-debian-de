WMLBASE  = .
SUBS = faq netiquette maps

INCLUDE = $(WMLBASE)/WML/INCLUDES
WMLOPT  = -I $(INCLUDE) -D IMGROOT=$(WMLBASE)/images -D DOCROOT="."

WMLFILES = $(wildcard *.wml)
HTMLFILES = $(patsubst %.wml,%.html,$(WMLFILES))

existing-SUBS := $(shell for dir in $(wildcard $(SUBS)) ''; do test -d $$dir && echo $$dir; done)
existing-SUBS-build := $(addsuffix -build,$(existing-SUBS))
existing-SUBS-clean := $(addsuffix -clean,$(existing-SUBS))
existing-SUBS-cleandest := $(addsuffix -cleandest,$(existing-SUBS))

all: $(HTMLFILES) fortunes.tgz $(existing-SUBS-build)

%.html: %.wml
	wml $(WMLOPT) -o $@ $<

fortunes.html fortunes-all.html: fortunes

fortunes.tgz: fortunes
	mkdir -p ftmp
	iconv -f latin1 -t UTF-8 < fortunes > ftmp/fortunes.channel.debian.de
	cd ftmp && strfile -s fortunes.channel.debian.de
	touch ftmp/fortunes.channel.debian.de.u8
	cd ftmp && tar zcf ../fortunes.tgz *
	rm -r ftmp

$(existing-SUBS-build):
	$(MAKE) -C $(subst -build,,$@)

clean: $(existing-SUBS-clean)
	rm -f $(HTMLFILES) fortunes.tgz
	rm -rf ftmp public

$(existing-SUBS-clean):
	$(MAKE) -C $(subst -clean,,$@) clean
