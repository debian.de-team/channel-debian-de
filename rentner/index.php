<?php
/**
 * $Id: index.php,v 1.17 2008-08-24 15:56:09 meebey Exp $
 * $Revision: 1.17 $
 * $Author: meebey $
 * $Date: 2008-08-24 15:56:09 $
 * 
 * Copyright (c) 2003 Debian Rentner Team  (#debian.de IRCnet) <http://channel.debian.de/rentner/>
 * 
 * Full GPL License: <http://www.gnu.org/licenses/gpl.txt>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
define('INDEX_REV',    '$Revision: 1.17 $');
define('INDEX_AUTHOR', '$Author: meebey $');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
	<head>
		<title>#debian.de IRCnet</title>
		<link rel="icon" href="/favicon.ico" type="image/ico">
		<meta name="Author" content="Mirco 'meebey' Bauer <mail@meebey.net>">
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-15">
		<meta name="keywords" content="Channel, Debian, Freenode, OFTC, eu-irc.net, IRCnet, Chat, chatten, Debian.de, deutsch, deutschsprachig, Linux, Support, Hilfe">
		<meta name="description" content="German speaking Channel about Debian GNU/Linux on Freenode, OFTC eu-irc.netand IRCnet.">
	</head>
	<body text="#000000" bgcolor="#FFFFFF" link="#0000FF" vlink="#800080" alink="#FF0000">
		<TABLE border="0" cellpadding="3" cellspacing="0" align="center" summary="">
			<TR>
				<TD>
					<img src="../images/debian.de.png" alt="" border="0" hspace="0" vspace="0" width="368" height="61">
				</TD>
			</TR>
		</TABLE>
		<TABLE border="0" cellpadding="0" cellspacing="0" width="100%" summary="">
			<TR bgcolor="#DF0451">
				<TD valign="top" width="15%" align="left"><img src="../images/corner-upperleft-df0451.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
				<TD>&nbsp;</TD>
				<TD valign="middle">
					<FONT size="+2" face="Helvetica,Arial" color="#ffffff">
						<b>#debian.de im IRCnet</b>
					</FONT>
				</TD>
				<TD align="right">
					<TABLE width="100%" border="0" cellpadding="0" cellspacing="0" summary="">
						<TR>
							<TD align="right" valign="top"><img src="../images/corner-upperright-df0451.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
						</TR>
						<TR>
							<TD align="right" valign="bottom"><img src="../images/corner-lowerright-df0451.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			<TR>
				<TD bgcolor="#DF0451">&nbsp;</TD>
				<TD align="left" valign="top" bgcolor="#ffffff"><img src="../images/corner-innerupperleft-df0451.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
				<TD colspan="2">&nbsp;</TD>
			</TR>
			<TR valign="bottom">
				<TD bgcolor="#DF0451">
					<TABLE width="100%" border="0" cellpadding="0" cellspacing="0" summary="">
						<TR>
							<TD align="left" valign="bottom"><img src="../images/corner-lowerleft-df0451.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
							<TD align="right" valign="bottom"><img src="../images/corner-lowerright-df0451.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
						</TR>
					</TABLE>
				</TD>
				<TD rowspan="3">&nbsp;</TD>
				<TD>
					&nbsp; &nbsp;
				</TD>
				<TD rowspan="3">&nbsp;</TD>
			</TR>
			<TR valign="top">
				<TD bgcolor="#FFFFFF">
					<BR>
					<TABLE width="100%" border="0" cellpadding="0" cellspacing="0" summary="">
						<TR bgcolor="#eeeeee" valign="top">
							<TD align="left" valign="top"><img src="../images/corner-upperleft-eeeeee.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
							<TD>&nbsp;</TD>
							<TD align="right" valign="top"><img src="../images/corner-upperright-eeeeee.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
						</TR>
						<TR bgcolor="#eeeeee">
							<TD>&nbsp;</TD>
							<TD>
								<P>
									<FONT face="Arial,Helvetica"><B><a href="http://channel.debian.de/rentner/">#debian.de.rentner</a></B>
								</P>
									<SMALL>
									&nbsp;<a href="index.php?section=rentner">Die Rentner</a><BR>
									&nbsp;<a href="index.php?section=liebeuser">Liebe User</a><BR>
									&nbsp;<a href="index.php?section=map">World Domination Map</a><BR>
									&nbsp;<a href="index.php?section=atbs">Abuse Reports</a><BR>
									&nbsp;<a href="index.php?section=credits">Credits</a><BR>
									&nbsp;<a href="https://www.df7cb.de/irc/debian.de/">Stats</a><BR>
									&nbsp;<a href="http://channel.debian.de/">#debian.de</a></BR>
									</SMALL>
								<P><B>Debian</B></P>
									<SMALL>
									&nbsp;<a href="http://www.de.debian.org/index.de.html">Debian Homepage</a><BR>
									</SMALL>
								</P></FONT>
							</TD>
							<TD>&nbsp;</TD>
						</TR>
						<TR bgcolor="#eeeeee" valign="bottom">
							<TD align="left" valign="bottom"><img src="../images/corner-lowerleft-eeeeee.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
							<TD>&nbsp;</TD>
							<TD align="right" valign="bottom"><img src="../images/corner-lowerright-eeeeee.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
						</TR>
					</TABLE>
					<BR>
				</TD>
				<TD>
					<FONT face="Helvetica,Arial" color="#000000">
<?php
if (isset($_GET['section'])) {
    $section = $_GET['section'];
} else {
    $section = 'home';
} 

switch ($section) {
    case 'rentner':
        include('rentner.inc.php');
        break;
    case 'liebeuser':
        include('liebeuser.inc.php');
        break;
    case 'map':
        include('map.inc.php');
        break;
    case 'atbs':
        include('atbs.inc.php');
        break;
    case 'credits':
        include('credits.inc.php');
        break;
    default:
        include('default.inc.php');
}
?>
					</FONT>
				</TD>
			</TR>
			<TR valign="top">
				<TD bgcolor="#BBDDFF">
					<TABLE width="100%" border="0" cellpadding="0" cellspacing="0" summary="">
						<TR valign="top">
							<TD align="left" valign="top"><img src="../images/corner-upperleft-bbddff.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
							<TD align="right" valign="top"><img src="../images/corner-upperright-bbddff.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
						</TR>
					</TABLE>
				</TD>
				<TD>&nbsp;</TD>
			</TR>
			<TR>
				<TD bgcolor="#BBDDFF">&nbsp;</TD>
				<TD align="left" valign="bottom" bgcolor="#ffffff"><img src="../images/corner-innerlowerleft-bbddff.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
				<TD colspan="2">&nbsp;</TD>
			</TR>
			<TR bgcolor="#BBDDFF">
				<TD width="15%" valign="bottom" align="left"><img src="../images/corner-lowerleft-bbddff.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
				<TD>&nbsp;</TD>
				<TD align="right">
					<FONT color="#DF0451">
<?php
$author = str_replace('Author', 'Autor', SECTION_AUTHOR);
$version = str_replace('$', '', SECTION_REV.' / '.$author); 
echo $version;
?>
					</FONT>
				</TD>
				<TD align="right">
					<TABLE width="100%" border="0" cellpadding="0" cellspacing="0" summary="">
						<TR>
							<TD align="right" valign="top"><img src="../images/corner-upperright-bbddff.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
						</TR>
						<TR>
							<TD align="right" valign="bottom"><img src="../images/corner-lowerright-bbddff.png" alt="" border="0" hspace="0" vspace="0" width="16" height="16"></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
	</body>
</html>
