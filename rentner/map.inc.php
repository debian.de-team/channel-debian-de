<?php
/**
 * $Id: map.inc.php,v 1.12 2007-12-30 17:37:56 tolimar Exp $
 * $Revision: 1.12 $
 * $Author: tolimar $
 * $Date: 2007-12-30 17:37:56 $
 * 
 * Copyright (c) 2003 Debian Rentner Team  (#debian.de IRCnet) <http://channel.debian.de/rentner/>
 * 
 * Full GPL License: <http://www.gnu.org/licenses/gpl.txt>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
define('SECTION_REV',    '$Revision: 1.12 $');
define('SECTION_AUTHOR', '$Author: tolimar $');
?>
<font size="+1"><b>The World Domination Map</b><br><br>
Auf dieser Karte ist verzeichnet wo auf der Welt sich unsere
Altersheime befinden. <br><br>
<a href="rentner.jpg"><img src="rentner.jpg" width="800"></a><br>
<b>Legende:</b> <font color=#F0F000>Rentner</font> <font color=#00FF00>Normale Channelmitglieder</font>
<br><br>
Die Daten dazu:
<br><br>
<b>Rentner</b>
<table border=1>
<tr>
<th>L&auml;nge</th><th>Breite</th><th>Nickname</th>
<?php 
    $fp = @fopen("rentner.txt", "r") or die ("Kann Datei nicht lesen.");
    while ($line = fgets($fp, 1024)) {
        if (strpos($line, "label_color") !== false) {
            continue;
        }
        print "<tr>\n";
        $infos = explode(" ", $line);
        print '<td>'.$infos[0].'</td><td>'.$infos[1].'</td><td>'.$infos[2].'</td>'."\n";
        print "</tr>\n";
    }
    fclose($fp);
?>
</table>
<br><br>
<b>Normale Channelmitglieder</b>
<table border=1>
<tr>
<th>L&auml;nge</th><th>Breite</th><th>Nickname</th>
<?php
    $fp = @fopen("rentner-user.txt", "r") or die ("Kann Datei nicht lesen.");
    while ($line = fgets($fp, 1024)) {
        if (strpos($line, "label_color") !== false) {
            continue;
        }
        print "<tr>\n";
        $infos = explode(" ", $line);
        print '<td>'.$infos[0].'</td><td>'.$infos[1].'</td><td>'.$infos[2].'</td>'."\n";
        print "</tr>\n";
    }
    fclose($fp);
?>
</table>

