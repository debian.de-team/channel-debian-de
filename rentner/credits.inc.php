<?php
/**
 * $Id: credits.inc.php,v 1.2 2007-12-30 17:37:56 tolimar Exp $
 * $Revision: 1.2 $
 * $Author: tolimar $
 * $Date: 2007-12-30 17:37:56 $
 * 
 * Copyright (c) 2003 Debian Rentner Team  (#debian.de IRCnet) <http://channel.debian.de/rentner/>
 * 
 * Full GPL License: <http://www.gnu.org/licenses/gpl.txt>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
define('SECTION_REV',    '$Revision: 1.2 $');
define('SECTION_AUTHOR', '$Author: tolimar $');
?>
Inhalt der Seite Copyright 2003 
<a href="mailto:mail@meebey.net">Mirco 'meebey' Bauer</a>, 
<a href="mailto:formorer@formorer.de">Alexander Wirt</a>, 
<a href="mailto:torsten@spasti.de">Torsten Raudssus</a>
<p>

Layout der Seite Copyright 2001-2002 Peter Palfrader
<a href="mailto:peter@palfrader.org">&lt;peter@palfrader.org&gt;</a> angelehnt an
das Layout der <a href="http://www.de.debian.org/index.de.html">Debian.org</a> Seiten. Deren License: <p>
<blockquote>
&copy 1997-2000 <a href="http://www.spi-inc.org/">Software in the Public Interest (SPI)</a>, 2001-2002 <a href="mailto:peter@palfrader.org">&lt;peter@palfrader.org&gt;</a>
<p>

This material may be distributed only subject to the terms and conditions set forth in the Open Publication
License, Draft v1.0 or later (the latest version is presently available at <a href="http://www.opencontent.org/openpub/">http://www.opencontent.org/openpub/</a>)
<p>
"Debian", the Debian Logo, "Open Hardware", and the Open Hardware Logo are trademarks of <a href="http://www.spi-inc.org/">Software in
the Public Interest, Inc.</a>
</blockquote>
