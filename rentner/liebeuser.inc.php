<?php
/**
 * $Id: liebeuser.inc.php,v 1.4 2007-12-30 17:37:56 tolimar Exp $
 * $Revision: 1.4 $
 * $Author: tolimar $
 * $Date: 2007-12-30 17:37:56 $
 * 
 * Copyright (c) 2003 Debian Rentner Team  (#debian.de IRCnet) <http://channel.debian.de/rentner/>
 * 
 * Full GPL License: <http://www.gnu.org/licenses/gpl.txt>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
define('SECTION_REV',    '$Revision: 1.4 $');
define('SECTION_AUTHOR', '$Author: tolimar $');
?>
<font size="+1"><b>"Liebe User" oder "Wieso seid ihr so assi drauf?!!?"</b></font><br><br>

Getty erkl&auml;rt einem User warum wir so ein unsoziales Verhalten manchmal an den Tag legen:<br>
<pre>
&lt;susn&gt;  sprech dich aus
&lt;Getty&gt; ich hab keine zeit fuer so spielchen
&lt;Getty&gt; du kannst nicht empfinden was wir empfinden, das kann ich dir auch
        nicht erzaehlen, aber suess wie du es interpretierst
&lt;susn&gt;  mir w�re das schon wichtig. Bei Dir glaube ich dass du es auch ehrlich 
        meinst
&lt;Getty&gt; er meint es auch ehrlich, das ist das problem was du nicht erkennst ;)
&lt;Getty&gt; stell dir vor, du musst 6 jahre lang am stueck mit solchen fragen von
        solchen idioten (ich erlaube mir das jetzt einfach mal so zu sagen) genervt
	zu werden
&lt;Getty&gt; und das schlimme ist, das wird ja nicht weniger sondern mehr, die 
        leute werden immer bloeder, und jeder hauch von dummheit muss bestraft
	werden, die leute muessen die eigenen impulse finden
&lt;Getty&gt; leider verstehen sie diese seitenhiebe dann nicht
&lt;susn&gt;  hmm
&lt;Getty&gt; wir haben keine zeit/lust uns damit auseinander zu setzen und wir
        haben gemerkt, ironie geht manchmal daneben, dann lieber knallharte worte,
	dann ist thema durch
&lt;Getty&gt; und noch was: dummerweise kann man wirklich fast alles (99,9%) von dem
        was wir hier sagen mit gezielten google begriffen oder dem was auf der
	debian platte vorhanden ist loesen
&lt;Getty&gt; und jetzt denk nochmal an die 6 jahre.....
&lt;Getty&gt; wenn du 6 jahre lang 99% aus dem internet vorliest oder von der debian
        platte abliest
&lt;Getty&gt; was meinste wie dann deine laune ist zu jede frage?
&lt;susn&gt;  aha
&lt;Getty&gt; kannste DAS nachvollziehen?
&lt;susn&gt;  ja
&lt;Getty&gt; ich will nicht sagen dass das jeden satz entschuldigt, aber es zeigt
        dass wir manchmal halt einfach nen anderen blickwinkel haben als der
	fragende
&lt;susn&gt;  ich bin halt kein Informatiker. und bin alleine mit Linux. Es ist auch
        so dass ich manchmal unsicher bin und vielleicht mir das eine oder andere
        nicht so recht zutraue. oder mir nicht zu helfen weiss
&lt;Getty&gt; ja das ist aber deni problem und nicht unser :) das ist es ja eben
&lt;susn&gt;  aber dennoch will ich Linux machen und es mehr und mehr beherrrschen
&lt;Getty&gt; wir wollen dich nicht zu linux treten, jeder ist fuer sich selbst 
        verantwortlich
&lt;susn&gt;  klar
&lt;Getty&gt; deswgen ist klar das wir dir !! 0 !! Motivation geben
&lt;Getty&gt; nein sogar umgekehrt, du musst UNS motivation geben deine fragen zu 
        beantworten
&lt;Getty&gt; und am meisten erfreuen wir uns immer wenn uns der fragende mit 
        erkenntnissen und selbsthilfe ueberrascht
&lt;susn&gt;  aha
&lt;susn&gt;  guter Tipp
&lt;susn&gt;  Danke sch�n
</pre>