<?php
/**
 * $Id: atbs.inc.php,v 1.4 2007-12-30 17:37:56 tolimar Exp $
 * $Revision: 1.4 $
 * $Author: tolimar $
 * $Date: 2007-12-30 17:37:56 $
 * 
 * Copyright (c) 2003 Debian Rentner Team  (#debian.de IRCnet) <http://channel.debian.de/rentner/>
 * 
 * Full GPL License: <http://www.gnu.org/licenses/gpl.txt>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
define('SECTION_REV',    '$Revision: 1.4 $');
define('SECTION_AUTHOR', '$Author: tolimar $');
?>
<table>
<tr><td>
<b><u>Spammer, DNS Abuser, Trolle in #debian.de und keine Ops in Sicht?</u></b><br>
<br>
<b>Fr&uuml;her:</b><br>
Mitleben und ignorieren. Hin- und wiedermal &uuml;ber die Ops aufregen.<br>
<br>
<b>Heute:</b><br>
"/msg atbs abuse User XY spammt im channel" Ops bekommen eine Nachricht,
und ergreifen die jeweiligen Ma&szlig;nahmen.<br>
Aber sie bekommen das Problem mit ohne gerade aktiv den Debian Channel
mitzulesen,
viele Ops sind im IRC aber nicht immer aktiv im Debian Channel. Mit dem
Abuse Report wird damit schnell und einfach die Information an den Ops
weitergegeben.
<br>
<br>
<b><u>atbs? Was, Wer, Wo, Wieso?</u></b><br>
<br>
atbs ist ein IRC bot, dieser stellt den Ops einige n&uuml;tzlige und heute
sogar notwendige Dienste zur Verf&uuml;gung.<br>
Fr&uuml;her ging es doch auch ohne Bots? Ja das ist richtig, aber da waren
auch noch keine 250 User im Debian Channel, das IRC wurde
nicht als Spam-Medium missbraucht und die meisten User im Internet waren
gebildete Menschen. Wir versto&szlig;en gegen unsere eigene Netiquette?
Nein, atbs ist _nicht_ im Debian Channel, das wird auch so bleiben! Dies
gilt genauso f&uuml;r andere Bots. Du siehst das anders? Andere Meinung?
Kein Problem, wir sind offen f&uuml;r Alles, aber bitte nicht in #debian.de,
sondern #debian.de.rentner. In #debian.de.rentner ist jeder willkommen und
dort werden alle Fragen/Administrativa gekl&auml;rt und beantwortet.
</tr></td>
</table>

